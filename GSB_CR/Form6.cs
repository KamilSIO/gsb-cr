﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        MySqlConnection connexion = new MySqlConnection("database=gsb; server=localhost; uid=root; Pwd=; SslMode=none;");

        private void Form6_Load(object sender, EventArgs e)
        {
            tb_NewCR_Date.Text = DateTime.Now.ToString();
            try
            {
                //On voit si la connexion fonctionne
                connexion.Open();
                MySqlCommand cmd;
                MySqlDataReader reader;

                string sql = "SELECT COL_NOM,COL_PRENOM FROM `collaborateur`";
                cmd = new MySqlCommand(sql, connexion);
                reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    cb_NewCR_NomColab.Items.Add(reader[0] + " " + reader[1]);
                }
                reader.Close();

                sql = "SELECT PRA_NOM,PRA_PRENOM FROM `praticien`";
                cmd = new MySqlCommand(sql, connexion);
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    cb_NewCR_NomPra.Items.Add(reader[0] + " " + reader[1]);
                }
                reader.Close();

            }
            catch (MySqlException co)
            {
                //En cas d'erreur, code d'erreur + Message perso
                MessageBox.Show(co.ToString());
                MessageBox.Show("Désolé la connexion n'a pas fonctionné");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[]splitPra = cb_NewCR_NomPra.Text.Split(' ');
            string nomPrati = splitPra[0];

            string[] splitCollab = cb_NewCR_NomColab.Text.Split(' ');
            string nomCollab = splitCollab[0];

            DateTime dateValue = DateTime.Parse(tb_NewCR_Date.Text);
            string formatForMySql = dateValue.ToString("yyyy-MM-dd HH:mm:ss");
            tb_NewCR_Date.Text = formatForMySql;
            
            try
            {
                //On voit si la connexion fonctionne
                MySqlCommand cmd;
                MySqlDataReader reader;

                string sql = "SELECT COL_MATRICULE FROM `collaborateur` WHERE COL_NOM ='" + nomCollab + "'";
                cmd = new MySqlCommand(sql, connexion);
                reader = cmd.ExecuteReader();

                reader.Read();
                string col_mat = reader[0].ToString();
                reader.Close();

                sql = "SELECT PRA_NUM FROM `praticien` WHERE PRA_NOM ='" + nomPrati +"'";
                cmd = new MySqlCommand(sql, connexion);
                reader = cmd.ExecuteReader();

                reader.Read();
                string num_pra = reader[0].ToString();
                reader.Close();

                sql = "INSERT INTO `compte_rendu`(`COL_MATRICULE`, `RAP_NUM`, `PRA_NUM`, `RAP_DATE`, `RAP_BILAN`, `RAP_MOTIF`)" +
                    " VALUES ('" + col_mat + "','" + tb_NewCR_NumRap.Text + "','" + num_pra + "','" +
                    tb_NewCR_Date.Text + "','" + rtb_NewCR_RapBilan.Text + "','" + rtb_NewCR_RapMotif.Text + "')";

                cmd = new MySqlCommand(sql, connexion);
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException co)
            {
                //En cas d'erreur, code d'erreur + Message perso
                MessageBox.Show(co.ToString());
                MessageBox.Show("Désolé la connexion n'a pas fonctionné");
            }
            this.Close();
        }
    }
}
