﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GSB_CR
{
    public partial class Form7 : Form

    {

        public Form7()
        {
            initializeComponent();
        }
        //Renseignement sur la base de donnée
        MySqlConnection connexion = new MySqlConnection("database=gsb; server=localhost; uid=root; Pwd=; SslMode=none;");

        private void Form7_Load(object sender, EventArgs e)
        {
            try
            {
                //Test connexion fonctionnelle
                connexion.Open();
                MySqlCommand cmd;
                MySqlDataReader reader;

                //Requête de recuperation des informations 
                string sql = "SELECT PRA_NUM,AVG(note_ecoute) FROM compte_rendu GROUP BY PRA_NUM";
                cmd = new MySqlCommand(sql, connexion);
                reader = cmd.ExecuteReader();
            }


            catch (MySqlException co)
            {
                //Si il y a une erreur, code d'erreur + Message perso
                MessageBox.Show(co.ToString());
                MessageBox.Show("Désolé la connexion n'a pas fonctionné");
            }
        }


            //Mise en place de la liste deroulante
            private void InitializeComponent()
            {

                this.comboBox1 = new System.Windows.Forms.ComboBox();
                this.addButton.Text = "Note";
                this.comboBox1.Items.AddRange(new object[] {"1/5",
                        "2/5",
                        "3/5",
                        "4/5",
                        "5/5"});
                this.comboBox1.Location = new System.Drawing.Point(8, 248);
                this.comboBox1.Size = new System.Drawing.Size(280, 21);
                this.comboBox1.TabIndex = 7;
            }



            private void btn_CR_Fermer_Click(object sender, EventArgs e)

            {
                this.Close();
            }
       

    }

}
